# NOTICE:
#
# Application name defined in TARGET has a corresponding QML filename.
# If name defined in TARGET is changed, the following needs to be done
# to match new name:
#   - corresponding QML filename must be changed
#   - desktop icon filename must be changed
#   - desktop filename must be changed
#   - icon definition filename in desktop file must be changed
#   - translation filenames have to be changed

# The name of your application
TARGET = sfos_remote

CONFIG += sailfishapp network sql
CONFIG += pillow_ssl

QT += sql

#CONFIG += precompile_header
#PRECOMPILED_HEADER = pch.h

SOURCES += src/sfos_remote.cpp \
    src/pillowcore/parser/parser.c \
    src/pillowcore/parser/http_parser.c \
    src/pillowcore/HttpServer.cpp \
    src/pillowcore/HttpHandler.cpp \
    src/pillowcore/HttpHelpers.cpp \
    src/pillowcore/HttpsServer.cpp \
    src/pillowcore/HttpHandlerSimpleRouter.cpp \
    src/pillowcore/HttpConnection.cpp \
    src/pillowcore/HttpHandlerProxy.cpp \
    src/pillowcore/HttpClient.cpp \
    src/pillowcore/HttpHeader.cpp \
    src/server.cpp \
    src/sailfish_api.cpp \
    src/http_handler_api.cpp

HEADERS += \
    src/pillowcore/parser/parser.h \
    src/pillowcore/parser/http_parser.h \
    src/pillowcore/HttpServer.h \
    src/pillowcore/HttpHandler.h \
    src/pillowcore/HttpHelpers.h \
    src/pillowcore/HttpsServer.h \
    src/pillowcore/HttpHandlerSimpleRouter.h \
    src/pillowcore/HttpConnection.h \
    src/pillowcore/HttpHandlerProxy.h \
    src/pillowcore/ByteArrayHelpers.h \
    src/pillowcore/private/ByteArray.h \
    src/pillowcore/HttpClient.h \
    src/pillowcore/pch.h \
    src/pillowcore/HttpHeader.h \
    src/pillowcore/PillowCore.h \
    src/server.h \
    src/sailfish_api.h \
    src/http_handler_api.h

OTHER_FILES += qml/sfos_remote.qml \
    qml/cover/CoverPage.qml \
    qml/pages/About.qml \
    qml/pages/AuthorizationDialog.qml \
    qml/pages/UI.qml \
    rpm/sfos_remote.changes.in \
    rpm/sfos_remote.spec \
    rpm/sfos_remote.yaml \
    translations/*.ts \
    sfos_remote.desktop \
    src/pillowcore/pillowcore.qbs

SAILFISHAPP_ICONS = 86x86 108x108 128x128 256x256

# to disable building translations every time, comment out the
# following CONFIG line
CONFIG += sailfishapp_i18n

# German translation is enabled as an example. If you aren't
# planning to localize your app, remember to comment out the
# following TRANSLATIONS line. And also do not forget to
# modify the localized app name in the the .desktop file.
TRANSLATIONS += translations/sfos_remote-de.ts


RESOURCES += \
    assets.qrc



