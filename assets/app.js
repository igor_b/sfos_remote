'use strict';

var api = function(url, params, cb, method, cb_err){
  if(method===undefined) method='GET';

  $.ajax(url, {data: params, method: method})
    .done(function(data){
      if(cb!==undefined) cb(data);
    })
    .fail(function(data){
      if(cb_err===undefined) window.app('auth');
      else cb_err(data);
    });
};

var key;
var apps;

var _tpl = function(html, vars){
	var escapes = {
		'&': '&amp;',
		'<': '&lt;',
		'>': '&gt;',
		'"': '&quot;',
		"'": '&#x27;',
		'/': '&#x2F;'
	};
  for(var v in vars){
    html = html.replace(new RegExp('\{\{'+v+'\}\}', 'g'), 
        (''+vars[v]).replace(/[&<>"'\/]/g, function(m){return escapes[m];})
    );
  }
  return html;
};


window.app.page('auth', function(){
  $('#login').on('click', function(){
    $(this).prop('disabled', true);
    $('#login-status').html('Access request sent. Confirm it on the mobile device.');
    $('#login-loader').show();
    $.get('/auth', {key: key})
      .done(function(d){
          window.app(apps[0].id, {});
      })
      .fail(function(){
        $('#menu ul li').remove();
          window.app("auth", {});
      });
  });

  return function(params){
    $('#login-loader').hide();
    $('#login-status').html('');
    $('#login').prop('disabled', false);
  };
});


window.app.page('browser', function(){
  var ul = $(this).find('ul');

  return function(params){
    api('/api/browser', {key:key}, function(d){
      ul.find('li').remove();
      $.each(d, function(i, e){
        e.date = new Date(e.date*1000);
        e.key = key;
        ul.html(ul.html()+_tpl('<li><img class="thumb" src="/api_file/?key={{key}}&path={{thumb_path}}"><h3>{{title}}</h3><small class="clearleft"><a href="{{url}}" target="_blank">{{url}}</a></small><small>opened: {{date}}</small></li>', e));
      });
    });
  };
});


window.app.page('notes', function(){
  var table = $(this).find('table');
  var $t = $(this);
  var data = {};

  $(this).on('notes:open', function(e, nid){
    var $td = e.target;
    var id = parseInt($td.data('id'));
    $td.find('a.notes-snip').slideUp();
    if(!$td.find('div.notes-edit').length){
      $td.append('<div class="notes-edit" style="display:none"><a href="#notes" class="notes-close close-button">close</a><textarea></textarea><button class="pure-button pure-button-primary">Save note</button> <a href="#notes" class="notes-close pure-button">Close</a></div>');
    }
    $td.find('div.notes-edit').slideDown();
    $('a.notes-close').on('click', function(){
      $td.trigger(jQuery.Event('notes:close', {target: $(this).closest('td')}));
      return false;
    });
    $td.find('textarea').val(data[id].text);
  });
  $(this).on('notes:close', function(e, nid){
    var $td = e.target;
    $td.find('a.notes-snip').slideDown();
    $td.find('div.notes-edit').slideUp();
  });

  return function(noteid){
    api('/api/notes', {key:key}, function(d){
      data = {};
      for(var dd in d){
        data[d[dd].id] = d[dd];
      }
      table.find('tr').remove();
      $.each(d, function(i, e){
        e.snip = e.text.substr(0, 90);
        if(e.text.length>50) e.snip += '...';
        e.cls='';
        table.html(table.html()+_tpl('<tr class="{{cls}}"><td class="id">{{id}}. <span class="notes-color" style="background-color: {{color}};"></span></td><td data-id="{{id}}" class="notes-content"><a class="notes-snip" data-id="{{id}}" href="#notes:{{id}}">{{snip}}</a></td></tr>', e));
      });
      $t.find('a.notes-snip').on('click', function(){
        $t.trigger(jQuery.Event('notes:open', {target: $(this).closest('td')}));
        return false;
      });
      if(noteid!==undefined){
        $t.trigger(jQuery.Event('notes:open', {target: $t.find('a[href="#notes:'+noteid+'"]')}));
      }
    });
  };
});


window.app.page('files', function(){
  var tbl = $(this).find('table');
  var current = "/home/nemo/";
  var files = [];

  var handle_click = function(e){
    var item = files[$(this).data('idx')];
    if(item.is_dir){
      current = escape(item.path)+'/'+escape(item.name);
      refresh_view();
      return false;
    }
    else{
      $(this).attr('href', '/api_file/?key='+key+'&path='+escape(item.path)+'/'+escape(item.name));
      $(this).attr('target', '_blank');
      return true;
    }
  };

  var refresh_view = function(){
    api('/api/files', {key:key, args: current}, function(d){
      files = d;
      tbl.find('tr').remove();
      $.each(d, function(i, e){
        if(e.name==='.') return;
        e.slash = e.is_dir?'/':'';
        e.perms = (e.is_readable?'r':'-')+(e.is_writable?'w':'-')+(e.is_executable?'x':'-');
        e.cls = e.is_dir?'files-dir':(e.is_file?'files-file':'files-other');
        if(e.size>1024) e.size_readable = Math.round(e.size/1024*100)/100+'Kb';
        if(e.size>1024*1024) e.size_readable = Math.round(e.size/1024/1024*100)/100+'Mb';
        if(e.size>1024*1024*1024) e.size_readable = Math.round(e.size/1024/1024/1024*100)/100+'Gb';
        if(e.is_dir) e.size_readable = '';
        e.idx = i;
        tbl.append(_tpl('<tr><td><a class="{{cls}}" href="#files:{{name}}" data-idx="{{idx}}">{{name}}{{slash}}</a></td><td>{{size_readable}}</td><td><span class="perms" title="Last modified: {{modified}}">{{perms}}</span></td></tr>', e));
      });
      if(d.length==2){
        tbl.append('<tr><td><i>empty directory</i></td><td></td><td></tr>');
      }
      tbl.find('a').on('click', handle_click);
      // update the url
      $('#fileupload').fileupload({url: '/upload?key='+key+'&path='+encodeURIComponent(current)});
    });
  }

  $('#fileupload').fileupload({
    url: undefined,  // to be populated on page refresh
    multipart: true,
    dataType: 'json',
    done: function (e, data) {
      $.each(data.result.files, function (index, file) {
        $('<p/>').text(file.name).appendTo('#files-list');
      });
      setTimeout(200, refresh_view);
    },
    progressall: function (e, data) {
      var progress = parseInt(data.loaded / data.total * 100, 10);
      $('#files-progress .progress-bar').css('width', progress + '%');
    }
  }).prop('disabled', !$.support.fileInput)
  .parent().addClass($.support.fileInput ? undefined : 'disabled');

  return function(params){
    refresh_view();
  };
});


// update navigation
$(document).on('page.hidden', function(e){
  $('#menu a[href="#'+e.target.id+'"]').closest('li').removeClass('pure-menu-selected');
});
$(document).on('page.shown', function(e){
  $('#menu a[href="#'+e.target.id+'"]').closest('li').addClass('pure-menu-selected');
});


$(function(){
  // fix css for displaying views
  var views = ['auth', 'notifications', 'contacts', 'browser', 'notes', 'files', 'clipboard'];
  // ...

  // add ambience colors
  $.get('/colors').done(function(d){
    $('#menu').css({'background': 'url("/background") no-repeat 50% 0', 'background-size': 'auto 150%'});
      $('#menuLink').css({'background': d.highlightColor});
    var css = "#menu li a.pure-menu-link{color: {{highlightColor}}; text-shadow: 0 0 4px rgba(0,0,0,0.3);} #menu .pure-menu li:not(.pure-menu-selected) a:hover{background-color: transparent; color: {{primaryColor}} !important;} #menu .pure-menu-selected{background-color: {{highlightBackgroundColor}};} #menu .pure-menu-selected a{color: {{primaryColor}} !important;}";
    $("<style type='text/css'>"+_tpl(css, d)+"</style>").appendTo("head");
  });

  // get list of apps
  $.get('/apps').done(function(d){
    apps = d;
    // populate menu
    $.each(apps, function(i, e){
      $('#menu ul').html($('#menu ul').html()+_tpl('<li class="pure-menu-item"><a href="#{{id}}" class="pure-menu-link">{{title}}</a></li>', e));
    });
  });

  // check cookie for key
  key = Cookies.get('key');
  if(key==null || key==undefined){
    key = Math.random().toString(36).substring(3);
    Cookies.set('key', key, {expires:2, secure:false});
  }

  // check if key valid
  $.get('/check_auth', {key: key}).done(function(){
    window.app('browser');
  }).fail(function(){
    window.app('auth');
  });

});

