# SFOS remote

SailfishOS application for transferring files, checking notes and other...

I started working on this in 2016 and due to the lack of time never finished it (code became too convoluted and I was too lazy to refactor it). The plan was to have complete web based phone "suite" which could manage files, notes, messages, contacts, notifications and other.


### Things that work:

- SSL secured connection (https:// must be excplicitely added to URL)
- ambiance colors in the web GUI
- access management (you'll need to approve the connection on the phone)
- file browsing, downloading and uploading
- reading notes
- checking open tabs from stock browser (with previews)

### Planned things:

- notes editing
- SMS message reading/writing
- contact management
- self-hosted relay server (so the phone doesn't need to be in the same network as browser)
- ...

Note: I haven't changed anything since 2016 (just commited uncommited changes). Interestingly, everything still works and I've used it on Jolla phone, Jolla C, Xperia X and now on Xperia XA2.

## Usage

- install the app from [https://bitbucket.org/igor_b/sfos_remote/downloads/sfos_remote-0.1-1.armv7hl.rpm](https://bitbucket.org/igor_b/sfos_remote/downloads/sfos_remote-0.1-1.armv7hl.rpm) or compile it yourself. After running it it will show you the current IP address with the option to enable or disable the server. 
- in your browser open https://ipaddress:10001 (you must include the https:// part)
- click Authorize
- confirm on the phone

I don't plan to work on this any more but forks and pull requests are welcome. The code is BSD licensed (it includes pillow lib which has Ruby license and I hope they are compatible).

## Screenshots
![app on the phone](https://bitbucket.org/igor_b/sfos_remote/raw/master/screenshots/1.png =100x)
![app on the phone](https://bitbucket.org/igor_b/sfos_remote/raw/master/screenshots/2.png)
![app on the phone](https://bitbucket.org/igor_b/sfos_remote/raw/master/screenshots/3.png)
![app on the phone](https://bitbucket.org/igor_b/sfos_remote/raw/master/screenshots/4.png)
![app on the phone](https://bitbucket.org/igor_b/sfos_remote/raw/master/screenshots/5.png)
![app on the phone](https://bitbucket.org/igor_b/sfos_remote/raw/master/screenshots/6.png)
![app on the phone](https://bitbucket.org/igor_b/sfos_remote/raw/master/screenshots/7.png)







