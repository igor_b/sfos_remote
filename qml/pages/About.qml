import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    Column {
        id: column

        width: page.width
        spacing: Theme.paddingLarge
        PageHeader {
            title: qsTr("SFOS remote UI")
        }
        Label {
            x: Theme.paddingLarge
            text: qsTr("About")
            color: Theme.secondaryHighlightColor
            font.pixelSize: Theme.fontSizeExtraLarge
        }
        TextArea {
            id: txtAbout
            width: parent.width
            readOnly: true
            wrapMode: TextEdit.WordWrap
            text: "Edit notes, see browser tabs, manage files and do other things on your device from your PC."
            color: Theme.secondaryColor
        }
        TextArea {
            id: txtNote
            width: parent.width
            readOnly: true
            wrapMode: TextEdit.WordWrap
            text: "Note: SailfishOS device needs to be in the same network as PC."
            color: Theme.secondaryColor
        }
        TextArea {
            id: txtAuthor
            width: parent.width
            readOnly: true
            wrapMode: TextEdit.WordWrap
            text: "Author: Igor Brkić <igor@hyperglitch.com>"
            color: Theme.secondaryColor
        }
    }
}
