import QtQuick 2.0
import Sailfish.Silica 1.0

Dialog {
    property string key
    property string address

    Column {
        width: parent.width

        DialogHeader {
            id: header
            title: "Authorize client"
        }

        TextArea {
            width: parent.width
            readOnly: true
            wrapMode: TextEdit.WordWrap
            text: "Authorize client from "+address+" (key '"+key+"')?"
        }
    }
}
