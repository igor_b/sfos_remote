/*
  Copyright (C) 2013 Jolla Ltd.
  Contact: Thomas Perl <thomas.perl@jollamobile.com>
  All rights reserved.

  You may use this file under the terms of BSD license as follows:

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Jolla Ltd nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import QtQuick 2.0
import Sailfish.Silica 1.0
import com.hg.server 1.0
import org.nemomobile.notifications 1.0

Page {
    id: mainUI
    property string statusProp
    property bool running: false

    // To enable PullDownMenu, place our content in a SilicaFlickable
    SilicaFlickable {
        anchors.fill: parent

        // PullDownMenu and PushUpMenu must be declared in SilicaFlickable, SilicaListView or SilicaGridView
        PullDownMenu {
            MenuItem {
                text: qsTr("About")
                onClicked: pageStack.push(Qt.resolvedUrl("About.qml"))
            }
        }

        // Tell SilicaFlickable the height of its content.
        contentHeight: column.height

        Notification {
            id: notification
            category: "x-nemo.example"
            summary: "Access validation required"
            body: "Acess validation required in SFOS remote"
            onClicked: console.log("Clicked")
        }


        Server{
            id: server
            onConnectionChanged: {
                var urls=[]
                for(var i in ips){
                    urls.push('https://'+ips[i]+':'+port);
                }
                statusText.text = status?"Listening on "+urls.join(', '):"Not active";
                statusProp = statusText.text
                serverSwitch.checked = server.listening;
                running: server.listening
            }

            onInit:{
                server.receiveAmbience(Theme.primaryColor, Theme.secondaryColor, Theme.highlightBackgroundColor, Theme.highlightColor, Theme.secondaryHighlightColor, Theme.backgroundImage);
            }

            onAuthorizationRequired: {
                notification.publish()
                var dialog = pageStack.push(Qt.resolvedUrl("AuthorizationDialog.qml"), {"key": key, "address": address});
                dialog.accepted.connect(function(){
                    server.authorize(key, true);
                })
                dialog.rejected.connect(function(){
                    server.authorize(key, false);
                })
            }
        }

        // Place our content in a Column.  The PageHeader is always placed at the top
        // of the page, followed by our content.
        Column {
            id: column

            width: mainUI.width
            spacing: Theme.paddingLarge
            PageHeader {
                title: qsTr("SFOS remote UI")
            }
            Label {
                x: Theme.paddingLarge
                text: qsTr("SFOS remote")
                color: Theme.secondaryHighlightColor
                font.pixelSize: Theme.fontSizeExtraLarge
            }
            TextSwitch{
                id: serverSwitch
                text: "Start server"
                description: ""
                onCheckedChanged: {
                    if(checked) server.startServer();
                    else server.stopServer();
                }
            }

            TextArea {
                id: statusText
                width: parent.width
                readOnly: true
                wrapMode: TextEdit.WordWrap
                text: "N/A"
                color: Theme.secondaryColor
            }
        }
    }
}


