#include "server.h"


Server::Server(QObject *parent) : QObject(parent){
    running = false;
    QTimer::singleShot(500, this, SLOT(startServer()));
    QTimer::singleShot(50, this, SLOT(initialized()));
}

void Server::startServer(){
    if(running) return;

    const int port = 10001;

    QFile certFile(":/ssl.crt");
    if(!certFile.open(QIODevice::ReadOnly)){
        qDebug() << "can't open certificate file";
        exit(3);
    }
    QFile keyFile(":/ssl.key");
    if(!keyFile.open(QIODevice::ReadOnly)){
        qDebug() << "can't open key file";
        exit(3);
    }

    QSslCertificate certificate(&certFile);
    QSslKey key(&keyFile, QSsl::Rsa);

    //server = new Pillow::HttpServer(QHostAddress(QHostAddress::Any), port);
    server = new Pillow::HttpsServer(certificate, key, QHostAddress(QHostAddress::Any), port);
    if(!server->isListening()) return;

    // list IP addresses
    QList<QString> ips;
    foreach (const QHostAddress &address, QNetworkInterface::allAddresses()) {
        if (address.protocol() == QAbstractSocket::IPv4Protocol && address != QHostAddress(QHostAddress::LocalHost)){
            ips << address.toString();
        }
    }

    Pillow::HttpHandlerStack* handler = new Pillow::HttpHandlerStack(server);
    new Pillow::HttpHandlerLog(handler);
    HttpHandlerAPI *api_handler = new HttpHandlerAPI(handler);
    api_handler->setAmbience(ambience);

    //new Pillow::HttpHandler404(handler);
    QObject::connect(server, SIGNAL(requestReady(Pillow::HttpConnection*)), handler, SLOT(handleRequest(Pillow::HttpConnection*)));

    // relay the signals to the handler
    QObject::connect(api_handler, SIGNAL(authorizationRequired(QString,QString)), this, SLOT(authRelay(QString,QString)));
    QObject::connect(this, SIGNAL(authorizationConfirmed(QString,bool)), api_handler, SLOT(authorize(QString,bool)));

    running = true;
    emit connectionChanged(running, ips, port);
    qDebug() << "Ready";
}

void Server::stopServer(){
    delete server;
    running = false;
    emit connectionChanged(running, QList<QString>(), 0);
    qDebug() << "server stopped";
}

bool Server::isListening(){
    return running;
}

void Server::authRelay(QString key, QString address){
    emit authorizationRequired(key, address);
}

void Server::authorize(QString key, bool auth){
    emit authorizationConfirmed(key, auth);
}

void Server::initialized(){
    emit init();
}

void Server::receiveAmbience(QString primaryColor, QString secondaryColor, QString highlightBackgroundColor, QString highlightColor, QString secondaryHighlightColor, QString backgroundImage){
    ambience["primaryColor"] = primaryColor;
    ambience["secondaryColor"] = secondaryColor;
    ambience["highlightBackgroundColor"] = highlightBackgroundColor;
    ambience["highlightColor"] = highlightColor;
    ambience["secondaryHighlightColor"] = secondaryHighlightColor;
    ambience["backgroundImage"] = backgroundImage.mid(7,-1);    // remove the "file://"
    qDebug() << "back: " << ambience["backgroundImage"];
}
