#include "sailfish_api.h"

#include<QDir>
#include<QDebug>
#include<QDateTime>


SailfishAPI::SailfishAPI(){}

QString SailfishAPI::apps(){
    return "[  \
          {\"title\": \"Browser\", \"id\": \"browser\"},        \
          {\"title\": \"Notes\", \"id\": \"notes\"},            \
          {\"title\": \"File manager\", \"id\": \"files\"}      \
        ]";
    /*
    return "[  \
          {title: 'Notifications', id: 'notifications'}, \
          {title: 'Contacts', id: 'contacts'},           \
          {title: 'Browser', id: 'browser'},             \
          {title: 'Notes', id: 'notes'},                 \
          {title: 'File manager', id: 'files'},          \
          {title: 'Clipboard', id: 'clipboard'}         \
        ]";
    */
}

QString SailfishAPI::handle(QStringList cmd, QString args){

    if(cmd[0]=="browser"){
        QString db_path = "/home/nemo/.local/share/org.sailfishos/sailfish-browser/sailfish-browser.sqlite";
        QString query = "select date, url, title, thumb_path from tab join tab_history on tab.tab_history_id=tab_history.id join link on tab_history.link_id=link.link_id";

        QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
        db.setDatabaseName(db_path);
        db.open();

        return sqlToJson(query, db_path);
    }
    else if(cmd[0]=="notes"){
        if(cmd.length()>1){
            qDebug() << "text edit: "<< args;
            return "{\"status\": \"OK\"}";
        }
        else{
        QDir d("/home/nemo/.local/share/jolla-notes/QML/OfflineStorage/Databases");
            d.setNameFilters(QStringList()<<"*.sqlite");
            QStringList fl = d.entryList();
            QString db_path = d.path()+"/"+fl[0];
            QFile ff(db_path);
            QString query = "select pagenr as id, color, body as text from notes order by id asc";
            return sqlToJson(query, db_path);
        }
    }
    else if(cmd[0]=="files"){
        QString path = "/home/nemo";
        if(args.length()>1){
            path = args;
        }
        QDir dir(path);
        if(!dir.canonicalPath().startsWith("/home/nemo")){
            dir.setPath("/home/nemo");
        }
        QJsonDocument json;
        QJsonArray filesArray;
        foreach(const QFileInfo &fi, dir.entryInfoList(QDir::Readable|QDir::Files|QDir::Dirs, QDir::DirsFirst|QDir::Name)){
            QJsonObject el;
            el.insert("name", fi.fileName());
            el.insert("is_dir", fi.isDir());
            el.insert("is_file", fi.isFile());
            el.insert("is_readable", fi.isReadable());
            el.insert("is_writable", fi.isWritable());
            el.insert("is_executable", fi.isExecutable());
            el.insert("is_symlink", fi.isSymLink());
            el.insert("modified", fi.lastModified().toLocalTime().toString());
            el.insert("size", fi.size());
            el.insert("path", fi.path());
            filesArray.push_back(el);
        }
        json.setArray(filesArray);
        return json.toJson();
    }
    return "ERR";
}


QString SailfishAPI::sqlToJson(const QString sqlquery, QString db_path){
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(db_path);
    db.open();

    QSqlQuery query(db);
    query.setForwardOnly(true);
    if(!query.exec(sqlquery)){
        return QString();
    }

    QJsonDocument  json;
    QJsonArray     recordsArray;

    while(query.next()){
        QSqlRecord r = query.record();
        QJsonObject recordObject;
        for(int x=0; x<r.count(); x++){
            recordObject.insert(r.fieldName(x),
                   QJsonValue::fromVariant(query.value(x)));
        }
        recordsArray.push_back(recordObject);
    }
    json.setArray(recordsArray);
    db.close();
    return json.toJson();
}






