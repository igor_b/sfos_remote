#ifndef HTTPHANDLERAPI_H
#define HTTPHANDLERAPI_H

#include "sailfish_api.h"

#include <QDebug>
#include <QObject>

#include <QFile>
#include <QFileInfo>
#include <QCryptographicHash>

#include "pillowcore/HttpServer.h"
#include "pillowcore/HttpConnection.h"
#include "pillowcore/HttpHandler.h"
#include "pillowcore/HttpHelpers.h"


class HttpHandlerAPI : public Pillow::HttpHandler{
    Q_OBJECT
    QVector<QString> approvedKeys;
    QHash<QString, Pillow::HttpConnection*> authWaitList;
    SailfishAPI* api;
    QHash<QString,QString> ambience;

    bool sendFile(QString path, Pillow::HttpConnection* connection, bool disposition=false);
    bool receiveFile(QString path, Pillow::HttpConnection *connection);

signals:
    void authorizationRequired(QString key, QString address);

public:

    HttpHandlerAPI(QObject* parent = 0);
    virtual bool handleRequest(Pillow::HttpConnection* rq);
    void setAmbience(QHash<QString,QString> ambience);

public slots:
    void authorize(QString key, bool auth=true);
};



#endif // HTTPHANDLERAPI_H
