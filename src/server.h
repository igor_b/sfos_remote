#ifndef SERVER_H
#define SERVER_H

#include <QObject>
#include <QtQml>
#include <QDebug>

#include <QNetworkInterface>

#include "pillowcore/HttpsServer.h"
#include "pillowcore/HttpConnection.h"
#include "pillowcore/HttpHandler.h"
#include "pillowcore/HttpHelpers.h"

#include "http_handler_api.h"


class Server : public QObject{
    Q_OBJECT
    Q_PROPERTY(bool listening READ isListening NOTIFY connectionChanged)


    bool running;
    QHash<QString,QString> ambience;
    Pillow::HttpsServer* server;

public:
    explicit Server(QObject *parent = 0);
    bool isListening();

signals:
    void connectionChanged(bool status, QList<QString> ips, int port);
    void authorizationRequired(QString key, QString address);
    void authorizationConfirmed(QString key, bool auth);
    void init();

public slots:
    void startServer();
    void stopServer();
    void authorize(QString key, bool auth);
    void authRelay(QString key, QString address);

    void initialized();
    void receiveAmbience(QString primaryColor, QString secondaryColor, QString highlightBackgroundColor, QString highlightColor, QString secondaryHighlightColor, QString backgroundImage);

};

#endif // SERVER_H
