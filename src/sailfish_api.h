#ifndef SAILFISHAPI_H
#define SAILFISHAPI_H

#include <QObject>
#include <QStringList>

#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlRecord>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonValue>
#include <QVariant>

class SailfishAPI{
    QString sqlToJson(const QString sqlquery, QString db_path);
public:
    SailfishAPI();
    static QString apps();
    QString handle(QStringList cmd, QString args);
};


#endif // SAILFISHAPI_H
