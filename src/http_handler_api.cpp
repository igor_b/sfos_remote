#include "http_handler_api.h"


HttpHandlerAPI::HttpHandlerAPI(QObject* parent) : Pillow::HttpHandler(parent){
    api = new SailfishAPI();
}

bool HttpHandlerAPI::handleRequest(Pillow::HttpConnection* rq){

    // get static contents (no authorization required)
    if(rq->requestPath()=="/"){
        return sendFile(":/assets/index.html", rq);
    }
    else if(rq->requestPath().startsWith("/assets/")){
        return sendFile(":"+rq->requestPath(), rq);
    }
    else if(rq->requestPath()=="/colors"){
        QString colors = "{      \
            \"highlightBackgroundColor\": \""+ambience["highlightBackgroundColor"]+"\", \
            \"highlightColor\":           \""+ambience["highlightColor"]+"\",           \
            \"primaryColor\":             \""+ambience["primaryColor"]+"\",             \
            \"secondaryColor\":           \""+ambience["secondaryColor"]+"\",           \
            \"secondaryHighlightColor\":  \""+ambience["secondaryHighlightColor"]+"\"   \
            }";
        Pillow::HttpHeaderCollection hh = Pillow::HttpHeaderCollection();
        hh << Pillow::HttpHeader("Content-Type", "application/json");
        rq->writeResponse(200, hh, colors.toUtf8());
        return true;
    }
    else if(rq->requestPath()=="/background"){
        return sendFile(ambience["backgroundImage"], rq);
    }
    else if(rq->requestPath()=="/apps"){
        Pillow::HttpHeaderCollection hh = Pillow::HttpHeaderCollection();
        hh << Pillow::HttpHeader("Content-Type", "application/json");
        rq->writeResponse(200, hh, SailfishAPI::apps().toUtf8());
        return true;
    }

    // authorization
    else if(rq->requestPath()=="/auth"){
        QString key = rq->requestParamValue("key");
        if(key.length()<4){
            rq->writeResponse(401, Pillow::HttpHeaderCollection(), "invalid key: "+rq->requestParamValue("key").toUtf8());
            return true;
        }
        // limit number of connected clients
        if(authWaitList.size()<3){
            authWaitList[key] = rq;
            emit authorizationRequired(key, rq->remoteAddress().toString());
        }
        return true;
    }
    else if(rq->requestPath()=="/check_auth"){
        if(approvedKeys.contains(rq->requestParamValue("key"))){
            rq->writeResponse(200, Pillow::HttpHeaderCollection(), "OK");
        }
        else{
            rq->writeResponse(401, Pillow::HttpHeaderCollection(), "auth fail");
        }
        return true;
    }


    // API router...
    else if(rq->requestPath().startsWith("/api/")){
        if(!approvedKeys.contains(rq->requestParamValue("key"))){
            rq->writeResponse(401, Pillow::HttpHeaderCollection(), "auth fail");
            return true;
        }
        QStringList api_cmd = QString(rq->requestPath()).mid(5).split("/", QString::SkipEmptyParts);
        QString r = api->handle(api_cmd, rq->requestParamValue("args"));

        Pillow::HttpHeaderCollection hh = Pillow::HttpHeaderCollection();
        hh << Pillow::HttpHeader("Content-Type", "application/json");

        rq->writeResponse(200, hh, r.toUtf8());
        return true;
    }
    else if(rq->requestPath().startsWith("/api_file/")){
        if(!approvedKeys.contains(rq->requestParamValue("key"))){
            rq->writeResponse(401, Pillow::HttpHeaderCollection(), "auth fail");
            return true;
        }
        return sendFile(rq->requestParamValue("path"), rq, true);
    }
    else if(rq->requestPath()=="/upload"){
        if(!approvedKeys.contains(rq->requestParamValue("key"))){
            rq->writeResponse(401, Pillow::HttpHeaderCollection(), "auth fail");
            return true;
        }
        qDebug() << "path: " << rq->requestParamValue("path");
        return receiveFile(rq->requestParamValue("path"), rq);
    }
    return false;
}

bool HttpHandlerAPI::sendFile(QString path, Pillow::HttpConnection* connection, bool disposition){
    const int bufferSize = 512*1024;
    QFileInfo resultPathInfo(path);
    QFile* file = new QFile(path);

    if (path[0]!=':' && !resultPathInfo.canonicalFilePath().startsWith("/home/nemo/")){
        return false; // Somebody tried to use some ".." or has followed symlinks that escaped out of the public path.
    }
    if(!resultPathInfo.exists()){
        connection->writeResponse(404, Pillow::HttpHeaderCollection(), QString("The requested resource '%1' is not accessible").arg(path).toUtf8());
        return true;
    }
    if(!file->open(QIODevice::ReadOnly)){
        // Could not read the file?
        connection->writeResponse(403, Pillow::HttpHeaderCollection(), QString("The requested resource '%1' is not accessible").arg(path).toUtf8());
        delete file;
    }
    else{
        Pillow::HttpHeaderCollection headers; headers.reserve(2);
        headers << Pillow::HttpHeader("Content-Type", Pillow::HttpMimeHelper::getMimeTypeForFilename(path));

        if(file->size() <= bufferSize){
            // The file fully fits in the supported buffer size. Read it and calculate an ETag for caching.
            QByteArray content = file->readAll();
            QCryptographicHash md5sum(QCryptographicHash::Md5); md5sum.addData(content);
            QByteArray etag = md5sum.result().toHex();
            if(disposition){
                headers << Pillow::HttpHeader("content-disposition", "attachment; filename=\"" + QFileInfo(file->fileName()).fileName().toUtf8() +"\"");
            }
            headers << Pillow::HttpHeader("ETag", etag);

            if (connection->requestHeaderValue("If-None-Match") == etag)
                connection->writeResponse(304); // The client's cached file was not modified.
            else
                connection->writeResponse(200, headers, content);

            delete file;
        }
        else{
            // The file exceeds the buffer size and must be sent incrementally. Do send the headers right away.
            headers << Pillow::HttpHeader("Content-Length", QByteArray::number(file->size()));
            if(disposition){
                headers << Pillow::HttpHeader("content-disposition", "attachment; filename=\"" + QFileInfo(file->fileName()).fileName().toUtf8() +"\"");
            }
            connection->writeHeaders(200, headers);

            Pillow::HttpHandlerFileTransfer* transfer = new Pillow::HttpHandlerFileTransfer(file, connection, bufferSize);
            file->setParent(transfer);
            connect(transfer, SIGNAL(finished()), transfer, SLOT(deleteLater()));
            transfer->writeNextPayload();
        }
    }

    return true;
}


bool HttpHandlerAPI::receiveFile(QString path, Pillow::HttpConnection *connection){
    // parse download files
    const QByteArray *cnt = &connection->requestContent();
    QByteArray tmp;
    long offset = 0;

    QFileInfo p(path);
    if (!p.canonicalFilePath().startsWith("/home/nemo/")){
        connection->writeResponse(401, Pillow::HttpHeaderCollection(), "auth err");
        return false; // Somebody tried to use some ".." or has followed symlinks that escaped out of the public path.
    }

    // first "line" is separator
    QByteArray separator = cnt->left(cnt->indexOf("\r\n"));

    while(offset < cnt->size()-separator.size()-2){
        QString filename = "";
        tmp.setRawData(cnt->constData()+offset, cnt->size()-offset);

        if(tmp.indexOf(separator)!=0)
            break;

        // skip the separator line and \r\n
        offset += separator.size()+2;

        // read headers
        while(true){
            tmp.setRawData(cnt->constData()+offset, cnt->size()-offset);
            QByteArray head = tmp.left(tmp.indexOf("\r\n"));
            offset += head.size()+2;
            if(head.startsWith("Content-Disposition")){
                // get filename
                int idx = head.indexOf("filename=\"");
                if(idx!=-1){
                    filename = head.mid(idx+10, head.size()-idx-10-1);
                }
            }
            if(head.size()==0){
                // empty line, data starts
                break;
            }
        }
        if(filename=="") break;
        tmp.setRawData(cnt->constData()+offset, cnt->size()-offset);
        QFile* f = new QFile(path+"/"+filename);
        f->open(QIODevice::WriteOnly);
        f->write(tmp, tmp.indexOf(separator)-2);
        f->close();
        offset += tmp.indexOf(separator);
        while((cnt->at(offset)=='\r' || cnt->at(offset)=='\n') && offset<cnt->size()) offset++;
    }
    connection->writeResponse(200, Pillow::HttpHeaderCollection(), "OK");
    return true;
}


void HttpHandlerAPI::authorize(QString key, bool auth){
    if(authWaitList.contains(key)){
        if(auth){
            approvedKeys << key;
            qDebug() << "Key '" << key << "' approved.";
            authWaitList[key]->writeResponse(200, Pillow::HttpHeaderCollection(), "OK");
        }
        else{
            qDebug() << "Key '" << key << "' rejected.";
            authWaitList[key]->writeResponse(401, Pillow::HttpHeaderCollection(), "auth fail");
        }
        authWaitList.remove(key);
    }
}

void HttpHandlerAPI::setAmbience(QHash<QString, QString> ambience){
    this->ambience = ambience;
}
